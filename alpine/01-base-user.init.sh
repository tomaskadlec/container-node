#!/bin/sh -e
# alpine/10-base-user.init.sh
# Creates user/group

# Handle group
if [ -z "$CONTAINER_GID" ]; then
    export CONTAINER_GID=$(getent group 'www-data' | cut -d: -f3)
fi

CONTAINER_GROUP=$(getent group "$CONTAINER_GID" | cut -d: -f1)
if [ -z "$CONTAINER_GROUP" ]; then
    CONTAINER_GROUP='container_group'
    addgroup -g "$CONTAINER_GID" "$CONTAINER_GROUP"
fi
export CONTAINER_GROUP

# Handle user
if [ -z "$CONTAINER_UID" ]; then
    export CONTAINER_UID=$(getent passwd 'www-data' | cut -d: -f 3)
fi

CONTAINER_USER=$(getent passwd "$CONTAINER_UID" | cut -d: -f 1)
if [ -z "$CONTAINER_USER" ]; then
    CONTAINER_USER='container_user'
    adduser -D -u "$CONTAINER_UID" -G "$CONTAINER_GROUP" "$CONTAINER_USER"
fi
export CONTAINER_USER

# ${system}/Dockerfile.${version}
# Add gosu and scripts to node:${version}-${system} image
# ${donotedit}

FROM node:${version}-${system}

RUN \
    apk add --no-cache bash

ENV GOSU_VERSION 1.11
RUN \
    apk add --no-cache --virtual .gosu-deps \
        dpkg \
        openssl \
    && dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true \
    && apk del .gosu-deps

# container init
ENV INIT_DIR=/usr/local/container_init
ADD ["init.sh", "*.init.sh", "/usr/local/container_init/"]
RUN chmod +x /usr/local/container_init/init.sh

# timezone
ENV TZ=Europe/Prague
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# add www-data user and group
RUN \
    sed -i '/^[^:]\+:[^:]*:33/d' /etc/group && \
    sed -i '/^[^:]\+:[^:]*:33/d' /etc/passwd && \
    echo 'www-data:x:33:www-data' >> /etc/group && \
    echo 'www-data:x:33:33:www-data:/var/www:/sbin/nologin' >> /etc/passwd

# change node/node UID/GID
RUN \
    sed -i 's/node:x:1000:1000/node:x:990:990/' /etc/passwd && \
    sed -i 's/node:x:1000/node:x:990/' /etc/group

RUN \
    mkdir -p /var/www && \
    chown www-data:www-data /var/www

# runtime configuration
ENTRYPOINT ["/bin/bash", "/usr/local/container_init/init.sh"]
CMD ["/bin/bash"]

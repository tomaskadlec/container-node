#!/bin/bash
# Handles container initialization

##
# Replace using a list of replacements
# $1 path to file
# $2 replacements in a form pattern<lf>replacement<lf>+
function replace {
    local file="$1"
    local replacements
    eval "declare -A replacements="${2#*=}
    local p_prefix="${3:-}"
    local p_suffix="${4:-}"
    local r_middle="${5:- = }"
    local patterns=''
    for pattern in "${!replacements[@]}"; do
        patterns="${patterns}s|${p_prefix}${pattern}${p_suffix}|${pattern}${r_middle}${replacements[$pattern]}|g;"
    done
    sed -i "$patterns" "$file"
}

INIT_PATH=$(readlink -f "$0")
INIT_DIR=$(dirname "$INIT_PATH")

# run all bundled init scripts
echo "container_init: source bundled scripts"
for INIT_SCRIPT in "$INIT_DIR/"*.init.sh; do
    if [ -f "$INIT_SCRIPT" ]; then
        echo "container_init: sourcing $INIT_SCRIPT"
        source "$INIT_SCRIPT"
    fi
done

# source application specific init scripts
echo "container_init: source application specific scripts"
if [ -n "$APP_DIR" -a -d "$APP_DIR" ]; then
    for INIT_SCRIPT in "$APP_DIR/bin/container_init/"*.init.sh; do
        if [ -f "$INIT_SCRIPT" ]; then
            echo "container_init: sourcing $INIT_SCRIPT"
            source "$INIT_SCRIPT"
        fi
    done
fi

# run CMD or user defined command
echo "container_init: GOSU $CONTAINER_USER($CONTAINER_UID) $CONTAINER_GROUP($CONTAINER_GID)"
echo "container_init: CMD $@"
exec gosu "$CONTAINER_USER" "$@"

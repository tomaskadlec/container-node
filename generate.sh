#!/bin/bash -e
# Generate node.js Dockerfiles. Optionally, build node.js images.

MODE='generate'

VERSIONS=(
    '20:alpine'
#    '18:alpine'
#    '16:alpine'
#    '14:alpine'
#    '12:alpine'
#    '10:alpine'
#    '8:alpine'
)

SYSTEMS=(
    'alpine'
)

##
# Build image
function build_image {
    export system="$1"
    export version="$2"
    export donotedit="This file was created automatically. Do not edit by hand!"
    export image="node:${version}-${system}"
    export dockerfile="${system}/Dockerfile.${version}"

    echo "==> Building $image"
    cat "${system}/Dockerfile.tpl" | envsubst '$system:$version:$image:$donotedit' > "$dockerfile"
    if [ "$MODE" = 'build' ]; then
        docker build --progress plain --no-cache --file "$dockerfile" --tag "registry.gitlab.com/tomaskadlec/container-node/${image}" "${system}/"
    fi
    cat >> .gitlab-ci.yml << -CI-
${system}-${version}:
    stage: node
    only:
        - master
    script:
        - docker build --progress plain --no-cache --file "$dockerfile" --tag "registry.gitlab.com/tomaskadlec/container-node/${image}" "${system}/"
        - docker push "registry.gitlab.com/tomaskadlec/container-node/${image}"

-CI-
}

OPTS=`getopt -o b --long build -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    -b | --build)
        MODE='build'
		shift
		;;
    * )
        break;
		;;
  esac
done

shift $((OPTIND))

# recreate .gitlab-ci.yml
cat .gitlab-ci.yml.tpl > .gitlab-ci.yml

# build different systems/versions
for SYSTEM in "${SYSTEMS[@]}"; do
    for NODE_VERSION in "${VERSIONS[@]}"; do
        VERSION="${NODE_VERSION%%:*}"
        if echo "$NODE_VERSION" | grep -vqs "$SYSTEM"; then
            echo "==> Skipping ${SYSTEM}/${VERSION}"
            continue
        fi
        build_image "$SYSTEM" "$VERSION";
    done
done

if [ "$BUILD" = 'build' ]; then
    # remove dangling images
    docker images --quiet --filter=dangling=true | xargs --no-run-if-empty docker rmi
fi

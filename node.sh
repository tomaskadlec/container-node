#!/bin/sh
# The script handles JavaScript dependencies and create.
# Important! The script is intended for development purposes use only.

SCRIPT_FILE=$(readlink -f $0)
SCRIPT_DIR=$(dirname "$SCRIPT_FILE")
APP_DIR="$SCRIPT_DIR/../../"

while sleep 10; do
    [[ -f ._lock ]] || break
    echo "Application is being initialized. Waiting ..." 1>&2
done

cd "$APP_DIR"

yarn install
yarn run encore dev

sh

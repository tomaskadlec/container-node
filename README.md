#container-node

[Node.js]: https://nodejs.org/
[node]: https://hub.docker.com/_/node/

This project builds container images with [Node.js]. The images are based on the official 
[node] images ([Alpine Linux]). The most significant change is a support to run processes
in the container as another user than `root`. The images provided by this project are used 
to build assets of various applications.

The project provides LTS versions of the images only. 

## Variables

``CONTAINER_GID``
    GID that should be used in container for running the command. Defaults
    to GIT of ``www-data`` group. If given GID does not exist a new group is
    created inside the container (called ``container_group``).

``CONTAINER_UID``
    UID that should be used in container for running the command. Defaults to
    UID of ``www-data`` user. If given UID does not exist a new user is created
    inside the container (called ``container_user``).

## License

[The MIT License]: LICENSE

This project is licensed under [The MIT License].

<!--
vim: spelllang=en spell textwidth=80 fo-=l: 
reformat paragraphs to 80 characters: select using v then hit gq
-->
